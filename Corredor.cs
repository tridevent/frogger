﻿using UnityEngine;
using System.Collections;

public class Corredor : MonoBehaviour {
    public Vector3 velocidad;
    public GameObject[] entradas;
    public GameObject[] salidas;
    public bool x;
    public bool y;
    public bool z;
    private int indice=0;
    public float aumentoVelocidad;
    public float tiempoAumento;
    public float velocidadMaxima;
	// Use this for initialization
	void Start () {
        InvokeRepeating("CambioVelocidad", tiempoAumento, tiempoAumento);
    }
	
	void Update () {
        transform.Translate(velocidad);
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag=="entrada")
        {
            indice = Random.Range(0, entradas.Length);
            if (x)
            {
                Debug.Log("xe");
                transform.position = salidas[indice].transform.position - new Vector3(transform.localScale.x*2, 0, 0);
            }
            if (y)
                transform.position = salidas[indice].transform.position - new Vector3(0, transform.localScale.y*2, 0);
            if (z)
                transform.position = salidas[indice].transform.position - new Vector3(0, 0, transform.localScale.z *2);
        }
        if (col.gameObject.tag == "salida")
        {
            if (x)
            {
                Debug.Log("xs");
                transform.position = entradas[indice].transform.position + new Vector3(transform.localScale.x*2, 0, 0);
                Debug.Log(transform.position);
            }
            if (y)
            {
                Debug.Log("ys");
                transform.position = entradas[indice].transform.position + new Vector3(0, transform.localScale.y*2, 0);
            }
            if (z)
            {
                Debug.Log("zs");
                transform.position = entradas[indice].transform.position + new Vector3(0, 0, transform.localScale.z*2);
            }
        }
    }
    void CambioVelocidad()
    {
        if (x)
            if (velocidad.x > 0)
            {
                if (velocidad.x < velocidadMaxima)
                    velocidad += new Vector3(aumentoVelocidad, 0, 0);
                else
                    velocidad.x = velocidadMaxima;
            }
            else
            {
                if (velocidad.x > velocidadMaxima)
                    velocidad -= new Vector3(aumentoVelocidad, 0, 0);
                else
                    velocidad.x = velocidadMaxima;
            }
        if (y)
            if (velocidad.y > 0)
            {
                if (velocidad.y < velocidadMaxima)
                    velocidad += new Vector3(0, aumentoVelocidad, 0);
                else
                    velocidad.y = velocidadMaxima;
            }
            else
            {
                if (velocidad.y > velocidadMaxima)
                    velocidad -= new Vector3(0, aumentoVelocidad, 0);
                else
                    velocidad.y = velocidadMaxima;
            }
        if (z)
            if (velocidad.z > 0)
            {
                if (velocidad.z < velocidadMaxima)
                    velocidad += new Vector3(0, 0, aumentoVelocidad);
                else
                    velocidad.z = velocidadMaxima;
            }
            else
            {
                if (velocidad.z > velocidadMaxima)
                    velocidad -= new Vector3(0, 0, aumentoVelocidad);
                else
                    velocidad.z = velocidadMaxima;
            } 
    }
}
