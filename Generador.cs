﻿using UnityEngine;
using System.Collections;

public class Generador : MonoBehaviour {
    public GameObject[] corredores;
    [HideInInspector]
    public bool reiniciarPosiciones;
    private Vector3[] posicionesGuardadas;
	void Start () {
        posicionesGuardadas = new Vector3[corredores.Length];
        for (int i = 0; i < corredores.Length; i++)
        {
            posicionesGuardadas[i] = corredores[i].transform.position;
        }
    }
	
	void Update () {
        if(reiniciarPosiciones)
        {
            for (int i = 0; i < corredores.Length; i++)
            {
                corredores[i].transform.position = posicionesGuardadas[i];
            }
            reiniciarPosiciones = false;
        }
	}
}
