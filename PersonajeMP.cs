﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PersonajeMP : MonoBehaviour {
    public Scrollbar controlador;
    public Vector3 posiciones;
    public GameObject generador;
    private float posicionActual=0;
    public Text vidasText;
    public int vidas = 3;
	// Use this for initialization
	void Start () {
        transform.position = new Vector3(posiciones.y, transform.position.y, transform.position.z);
    }
	
	// Update is called once per frame
	void Update () {
        if(vidas<1)
        {
            Debug.LogError("Perdió el jugador " + gameObject.name);
        }
        if(controlador.value != posicionActual)
        {
            if (controlador.value == 0)
            {
                //izq
                transform.position = new Vector3(posiciones.x, transform.position.y, transform.position.z);
            }
            if (controlador.value != 0 && controlador.value != 1)
            {
                //centro
                transform.position = new Vector3(posiciones.y, transform.position.y, transform.position.z);
            }
            if (controlador.value == 1)
            {
                //der
                transform.position = new Vector3(posiciones.z, transform.position.y, transform.position.z);
            }
            posicionActual = controlador.value;
        }
        else
        {
            transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
            transform.position = new Vector3(transform.position.x - 0.1f, transform.position.y, transform.position.z);
        }
    
	}
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "corredor")
        {
            generador.GetComponent<Generador>().reiniciarPosiciones = true;
            vidas--;
            vidasText.text = vidas.ToString();
        }
    }
}
