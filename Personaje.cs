﻿using UnityEngine;
using System.Collections;

public class Personaje : MonoBehaviour {
    public int velocidad;
    public bool principal;
    public GameObject objetivo;
    public GameObject objetivoContrario;
	void Start () {

	}
	
	void Update () {
        Vector3 ejex = Input.GetAxis("Horizontal") * transform.right * Time.deltaTime * velocidad;
        Vector3 ejez = Input.GetAxis("Vertical") * transform.forward * Time.deltaTime * velocidad;
        transform.Translate(ejex + ejez);
    }
    void OnTriggerEnter(Collider col)
    {
        if(principal)
        {
            if (col.gameObject.tag == "objetivo")
            {
                Debug.LogError("Gane");
            }
        }
        else
        {
            if (col.gameObject.tag == "objetivoContrario")
            {
                Debug.LogError("Gane");
            }
        }
    }
    void OnCollisionEnter( Collision col)
    {
        if(col.gameObject.tag=="corredor")
        {
            transform.position = objetivoContrario.transform.position;
        }
    }
}
